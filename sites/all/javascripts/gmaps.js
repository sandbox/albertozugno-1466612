//<![CDATA[
      

function gmapsload() {
if (GBrowserIsCompatible()) 
	{
	
	  var gmarkers = [];
      var htmls = [];
      var to_htmls = [];
      var from_htmls = [];
      var i=0;
      
      
      var baseIcon = new GIcon();
          baseIcon.iconSize=new GSize(32,32);
          baseIcon.shadowSize=new GSize(56,32);
          baseIcon.iconAnchor=new GPoint(16,32);
          baseIcon.infoWindowAnchor=new GPoint(16,0);
          
      var duomo = new GIcon(baseIcon, "http://maps.google.com/mapfiles/kml/pal2/icon11.png", null, "http://maps.google.com/mapfiles/kml/pal2/icon11s.png");
      var parking = new GIcon(baseIcon, "http://maps.google.com/mapfiles/ms/micons/parkinglot.png", null, "http://maps.google.com/mapfiles/ms/micons/parkinglot.shadow.png");
      var studio = new GIcon(baseIcon, "http://maps.google.com/mapfiles/ms/micons/grn-pushpin.png", null, "http://maps.google.com/mapfiles/ms/micons/pushpin_shadow.png");
    
     // http://www.visual-case.it/cgi-bin/vc/GMapsIcons.pl


// Crea un nuovo marker nel punto specificato con una descrizione HTML associata
function createMarker(point,name,html,icon) {
var marker = new GMarker(point,icon);

 // The info window version with the "to here" form open
        to_htmls[i] = html + '<br>Indicazioni: <b>A qui<\/b> - <a href="javascript:fromhere(' + i + ')">Da qui<\/a>' +
           '<br>Indirizzo di partenza:<form action="http://maps.google.com/maps" method="get" target="_blank">' +
           '<input type="text" SIZE=40 MAXLENGTH=40 name="saddr" id="saddr" value="" /><br>' +
           '<INPUT value="Ottieni Indicazioni" TYPE="SUBMIT">' +
           '<input type="hidden" name="daddr" value="' + point.lat() + ',' + point.lng() + 
                   "(" + name + ")" + 
           '"/>';
        // The info window version with the "from here" form open
        from_htmls[i] = html + '<br>Direzioni: <a href="javascript:tohere(' + i + ')">A qui<\/a> - <b>Da qui<\/b>' +
           '<br>Indirizzo d\'arrivo:<form action="http://maps.google.com/maps" method="get"" >' +
           '<input type="text" SIZE=40 MAXLENGTH=40 name="daddr" id="daddr" value="" /><br>' +
           '<INPUT value="Ottieni Indicazioni" TYPE="SUBMIT">' +
           '<input type="hidden" name="saddr" value="' + point.lat() + ',' + point.lng() +
                   "(" + name + ")" + 
           '"/>';
        // The inactive version of the direction info
        html = html + '<br><a href="http://maps.google.com/maps?saddr=&daddr=' + point.toUrlValue() + " (" + name + ")" + '" target ="_blank">Indicazioni Stradali<\/a>';
        //Indicazioni: <a href="javascript:tohere('+i+');">A qui<\/a> - <a href="javascript:fromhere('+i+');">Da qui<\/a>';


GEvent.addListener(marker, "mouseover", function() {
  marker.openInfoWindowHtml(html);
});
gmarkers[i] = marker;
htmls[i] = html;
i++;
return marker;


}



// functions that open the directions forms
function tohere(i) {
gmarkers[i].openInfoWindowHtml(to_htmls[i]);
}
function fromhere(i) {
gmarkers[i].updateInfoWindow(from_htmls[i]);
}
      
      
      
      
       
 var map = new GMap2(document.getElementById("gmap"));
 map.setCenter(new GLatLng(45.666261, 12.241462), 16);
 map.setUIToDefault();
 map.enableContinuousZoom();
 map.disableScrollWheelZoom();
 map.addControl(new GOverviewMapControl()) ;





//  Markers  nella mappa
map.addOverlay(
createMarker(new GLatLng(45.666261, 12.241462),'Studio Fiorese e Manzato', '<b>Studio Fiorese & Manzato</b><br />Piazza ex convento Cappucine 8<br />Treviso, 31100 TV',studio)
);

map.addOverlay(
createMarker(new GLatLng(45.666343, 12.243007),'Duomo di Treviso', '<b>Duomo di Treviso</b>',duomo)
);

map.addOverlay(
createMarker(new GLatLng(45.664469, 12.241323),'Parcheggio', 'Parcheggio verosimile',parking)
);

}

	    else { alert("Sorry, the Google Maps API is not compatible with this browser");}

	}

/**
        
        // Create our "tiny" marker icon
        var blueIcon = new GIcon(G_DEFAULT_ICON);
        blueIcon.image = "http://gmaps-samples.googlecode.com/svn/trunk/markers/blue/blank.png";
		
		// Set up our GMarkerOptions object
		markerOptions = { icon:blueIcon };

        // Add 10 markers to the map at random locations
        var bounds = map.getBounds();
        var southWest = bounds.getSouthWest();
        var northEast = bounds.getNorthEast();
        var lngSpan = northEast.lng() - southWest.lng();
        var latSpan = northEast.lat() - southWest.lat();
        for (var i = 0; i < 10; i++) {
          var latlng = new GLatLng(southWest.lat() + latSpan * Math.random(),
                                  southWest.lng() + lngSpan * Math.random());
          map.addOverlay(new GMarker(latlng, markerOptions));
        }
      }
    }

*/

//]]>
